# Anemone examples, tdFIR directory, top level
# $Id: Makefile 9940 2012-12-28 18:22:29Z clund@localk.com $

SUBDIRS := $(wildcard */)

.PHONY : all
all:
	echo "Please cd into each directory to run make"

.PHONY : clean
clean:
	-rm -rf *~
	for d in $(SUBDIRS);\
        do                  \
        $(MAKE) --directory=$$d clean;\
        done
