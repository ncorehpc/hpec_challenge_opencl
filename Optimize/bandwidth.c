// SAR Benchmark, tdFIR, scalar
// $Id$

// This version of tdFIR.c does no math.
// It exists to determine if other programs are memory bound.

#include "tdFIR.h"
#include <complex.h>

void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{
  int Element;

  for (Element = 0; Element < N; Element++)
    OutputArray[Element] = InputArray[Element];

  for (Element = 0; Element < K-1; Element++)
    OutputArray[Element+N] = FilterArray[Element];

  return;
}
