// SAR Benchmark, tdFIR, scalar
// $Id: pointers.c 9940 2012-12-28 18:22:29Z clund@localk.com $

// This version of tdFIR.c has byte pointers and does not use complex

#include "tdFIR.h"
#include <complex.h>

// Process a single complex output
// Note the access pattern:
//   Calls to this function often use the entire FilterArray
//   Calls often walk the InputArray backwards by up to K elements
static inline void tdFIRwork
  ( unsigned char const *       restrict InputElement,
    unsigned char       * const restrict OutputElement,
    unsigned short                       FilterCount,
    unsigned char const *       restrict FilterElement
  )
{ float sumReal = 0.0f, sumImag = 0.0f;
  
  for ( ; FilterCount !=0 ; FilterCount-- )
  { // Load this filter element into registers
    const float FilterReal = *(float *)FilterElement;
    const float FilterImag = *(float *)(FilterElement + sizeof(float));

    // Load this input element into registers       
    const float InputReal = *(float *)InputElement;
    const float InputImag = *(float *)(InputElement + sizeof(float));

    // Complex multiply
    sumReal += InputReal*FilterReal - InputImag*FilterImag;
    sumImag += InputReal*FilterImag + InputImag*FilterReal;

    FilterElement += sizeof(float complex);
    InputElement -= sizeof(float complex);

  };

  // Store the sum
  *(float complex *)OutputElement = sumReal + I*sumImag;

  return;
}

// Note in this implementation
//   K must be an even number greater than zero
//   N must be greater than K
void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{ // Variables shared by all three loops
  unsigned char       * restrict OutputElement = (unsigned char *)OutputArray;
  unsigned char const * restrict InputBase     = (unsigned char *)InputArray;

  // Process first K-1 elements differently
  for ( unsigned short FilterCount = 1; FilterCount < K; FilterCount++ )
  { tdFIRwork (InputBase, OutputElement, FilterCount, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // This is the main loop
  for ( unsigned short OutputCount = N - K; OutputCount != 0; OutputCount--)
  { tdFIRwork (InputBase, OutputElement, K, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // Process one element as above then do the last K-1 elements differently
  { unsigned char const * restrict FilterBase = (unsigned char *)FilterArray;
    for ( unsigned short FilterCount = K; FilterCount != 0; FilterCount--)
    { tdFIRwork (InputBase, OutputElement, FilterCount, FilterBase);
      OutputElement += sizeof(float complex);
      FilterBase += sizeof(float complex);
      // Note that we no longer increment InputBase
    }
  }

return;

} // End tdFIR function
