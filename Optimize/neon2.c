// SAR Benchmark, tdFIR. scalar
// $Id: sse3.c 9965 2013-01-09 02:31:01Z clund@localk.com $

// This version of tdFIR.c uses SSE3 instructions

#include "tdFIR.h"
#include <complex.h>
#include <arm_neon.h> // For NEON functions

// Process a single complex output
// Note the access pattern:
//   Calls to this function often use the entire FilterArray
//   Calls often walk the InputArray backwards by up to K elements
static inline void tdFIRwork
  ( unsigned char const *       restrict InputElement,
    unsigned char       * const restrict OutputElement,
    unsigned short                       FilterCount,
    unsigned char const *       restrict FilterElement
  )
{ float complex sum;
  float32x2_t sumSIMDreal = vdup_n_f32(0.0f);
  float32x2_t sumSIMDimag = vdup_n_f32(0.0f);
  unsigned short FilterDoubleCount = FilterCount / 2;
  InputElement -= sizeof(float complex);  // Primes the loop. Remember this counts backwards.
  // Start with a loop manually unrolled once
  for ( ; FilterDoubleCount != 0; FilterDoubleCount-- )
  { // Load this filter element into registers
    float32x2x2_t FilterPair;
    FilterPair = vld2_f32 ((float *)FilterElement); // loads F0, F1 in a register pair

    // Load this input element into registers
    float32x2x2_t InputPair;
    InputPair = vld2_f32 ((float *)InputElement); // loads I(-1), I0 in a register pair
    // Correct for the fact the scalar algorithm loads the elements in the opposite order
    InputPair.val[0] = vrev64_f32(InputPair.val[0]); // creates I0, I(-1) for real pair
    InputPair.val[1] = vrev64_f32(InputPair.val[1]); // creates I0, I(-1) for imag pair
  
    // Complex multiply
    // (x+yi) + (u+vi) = (xu-yv) + (xv+yu)i
    sumSIMDreal = vmla_f32(sumSIMDreal, InputPair.val[0], FilterPair.val[0]); // +xu
    sumSIMDreal = vmls_f32(sumSIMDreal, InputPair.val[1], FilterPair.val[1]); // -yv
    sumSIMDimag = vmla_f32(sumSIMDimag, InputPair.val[0], FilterPair.val[1]); // +xv
    sumSIMDimag = vmla_f32(sumSIMDimag, InputPair.val[1], FilterPair.val[0]); // +yu

    FilterElement += 2*sizeof(float complex);
    InputElement -= 2*sizeof(float complex);
  };
  
  // Need to sum across the vector
  sumSIMDreal = vpadd_f32(sumSIMDreal, sumSIMDreal); // r0+r1, r0+r1
  sumSIMDimag = vpadd_f32(sumSIMDimag, sumSIMDimag); // i0+i1, i0+i1
  sum = vget_lane_f32(sumSIMDreal, 0) + vget_lane_f32(sumSIMDimag, 0)*I;
   
  // If FilterCount was odd, we need an extra, pass
  if ( (FilterCount % 2) != 0 )
  { // Load this filter element into registers
    const float FilterReal = *(float *)FilterElement;
    const float FilterImag = *(float *)(FilterElement + sizeof(float));

    // Load this input element into registers 
    InputElement += sizeof(float complex);  // Unprimes the first loop.
    const float InputReal = *(float *)InputElement;
    const float InputImag = *(float *)(InputElement + sizeof(float));

    // Complex multiply
    sum += InputReal*FilterReal - InputImag*FilterImag;
    sum += I*(InputReal*FilterImag + InputImag*FilterReal);

  };

  // Store the sum
 *(float complex *)OutputElement = sum;

  return;
}

// Note in this implementation
//   K must be an even number greater than zero
//   N must be greater than K
void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{ // Variables shared by all three loops
  unsigned char       * restrict OutputElement = (unsigned char *)OutputArray;
  unsigned char const * restrict InputBase     = (unsigned char *)InputArray;

  // Process first K-1 elements differently
  for ( unsigned short FilterCount = 1; FilterCount < K; FilterCount++ )
  { tdFIRwork (InputBase, OutputElement, FilterCount, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // This is the main loop
  for ( unsigned short OutputCount = N - K; OutputCount != 0; OutputCount--)
  { tdFIRwork (InputBase, OutputElement, K, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // Process one element as above then do the last K-1 elements differently
  { unsigned char const * restrict FilterBase = (unsigned char *)FilterArray;
    for ( unsigned short FilterCount = K; FilterCount != 0; FilterCount--)
    { tdFIRwork (InputBase, OutputElement, FilterCount, FilterBase);
      OutputElement += sizeof(float complex);
      FilterBase += sizeof(float complex);
      // Note that we no longer increment InputBase
    }
  }

return;

} // End tdFIR function
