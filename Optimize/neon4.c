// SAR Benchmark, tdFIR. scalar
// $Id: sse3.c 9965 2013-01-09 02:31:01Z clund@localk.com $

// This version of tdFIR.c uses SSE3 instructions

#include "tdFIR.h"
#include <complex.h>
#include <arm_neon.h> // For NEON functions

// Process a single complex output
// Note the access pattern:
//   Calls to this function often use the entire FilterArray
//   Calls often walk the InputArray backwards by up to K elements
static inline void tdFIRwork
  ( unsigned char const *       restrict InputElement,
    unsigned char       * const restrict OutputElement,
    unsigned short                       FilterCount,
    unsigned char const *       restrict FilterElement
  )
{ float complex sum;
  float32x4_t sumSIMDreal4 = vdupq_n_f32(0.0f);
  float32x4_t sumSIMDimag4 = vdupq_n_f32(0.0f);
  unsigned short FilterQuadCount = FilterCount / 4;
  InputElement -= 3*sizeof(float complex);  // Primes the loop. Remember this counts backwards.
  // Start with a loop manually unrolled once
  for ( ; FilterQuadCount != 0; FilterQuadCount-- )
  { // Load this filter element into registers
    float32x4x2_t FilterQuads;
    FilterQuads = vld2q_f32 ((float *)FilterElement); // loads F0, F1, F2, F3 in two Q registers (real, imag)

    // Load this input element into registers
    float32x4x2_t InputQuads;
    InputQuads = vld2q_f32 ((float *)InputElement); // loads I(-3), I(-2), I(-1), I0 in two Q registers (real, imag)
    // Correct for the fact the scalar algorithm loads the elements in the opposite order
    InputQuads.val[0] = vcombine_f32(vget_high_f32(InputQuads.val[0]), vget_low_f32(InputQuads.val[0]));
    InputQuads.val[0] = vrev64q_f32(InputQuads.val[0]); // creates I0, I(-1), I(-2), I(-3) for real quad
    InputQuads.val[1] = vcombine_f32(vget_high_f32(InputQuads.val[1]), vget_low_f32(InputQuads.val[1]));
    InputQuads.val[1] = vrev64q_f32(InputQuads.val[1]); // creates I0, I(-1), I(-2), I(-3) for imag quad
   
    // Complex multiply
    // (x+yi) + (u+vi) = (xu-yv) + (xv+yu)i
    sumSIMDreal4 = vmlaq_f32(sumSIMDreal4, InputQuads.val[0], FilterQuads.val[0]); // +xu
    sumSIMDreal4 = vmlsq_f32(sumSIMDreal4, InputQuads.val[1], FilterQuads.val[1]); // -yv
    sumSIMDimag4 = vmlaq_f32(sumSIMDimag4, InputQuads.val[0], FilterQuads.val[1]); // +xv
    sumSIMDimag4 = vmlaq_f32(sumSIMDimag4, InputQuads.val[1], FilterQuads.val[0]); // +yu

    FilterElement += 4*sizeof(float complex);
    InputElement -= 4*sizeof(float complex);
  };
  
  // Need to sum across the vector
  float32x2_t sumSIMDreal2 = vadd_f32(vget_high_f32(sumSIMDreal4), vget_low_f32(sumSIMDreal4)); // r0+r2, r1+r3
  sumSIMDreal2 = vpadd_f32(sumSIMDreal2, sumSIMDreal2); // (r0+r2) + (r1+r3), repeated
  float32x2_t sumSIMDimag2 = vadd_f32(vget_high_f32(sumSIMDimag4), vget_low_f32(sumSIMDimag4)); // i0+i2, i1+i3
  sumSIMDimag2 = vpadd_f32(sumSIMDimag2, sumSIMDimag2); // (i0+i2) + (i1+i3), repeated
  sum = vget_lane_f32(sumSIMDreal2, 0) + vget_lane_f32(sumSIMDimag2, 0)*I;
   
  // If FilterCount was odd, we need an extra, pass
  unsigned short FilterExtra = FilterCount % 4;
  InputElement += 3*sizeof(float complex);
  for ( ; FilterExtra != 0; FilterExtra--)
  { // Load this filter element into registers
    const float FilterReal = *(float *)FilterElement;
    const float FilterImag = *(float *)(FilterElement + sizeof(float));

    // Load this input element into registers 
    const float InputReal = *(float *)InputElement;
    const float InputImag = *(float *)(InputElement + sizeof(float));

    // Complex multiply
    sum += InputReal*FilterReal - InputImag*FilterImag;
    sum += I*(InputReal*FilterImag + InputImag*FilterReal);

    FilterElement += sizeof(float complex);
    InputElement -= sizeof(float complex);
  };

  // Store the sum
 *(float complex *)OutputElement = sum;

  return;
}

// Note in this implementation
//   K must be an even number greater than zero
//   N must be greater than K
void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{ // Variables shared by all three loops
  unsigned char       * restrict OutputElement = (unsigned char *)OutputArray;
  unsigned char const * restrict InputBase     = (unsigned char *)InputArray;

  // Process first K-1 elements differently
  for ( unsigned short FilterCount = 1; FilterCount < K; FilterCount++ )
  { tdFIRwork (InputBase, OutputElement, FilterCount, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // This is the main loop
  for ( unsigned short OutputCount = N - K; OutputCount != 0; OutputCount--)
  { tdFIRwork (InputBase, OutputElement, K, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // Process one element as above then do the last K-1 elements differently
  { unsigned char const * restrict FilterBase = (unsigned char *)FilterArray;
    for ( unsigned short FilterCount = K; FilterCount != 0; FilterCount--)
    { tdFIRwork (InputBase, OutputElement, FilterCount, FilterBase);
      OutputElement += sizeof(float complex);
      FilterBase += sizeof(float complex);
      // Note that we no longer increment InputBase
    }
  }

return;

} // End tdFIR function
