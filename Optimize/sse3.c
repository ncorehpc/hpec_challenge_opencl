// SAR Benchmark, tdFIR. scalar
// $Id: sse3.c 9965 2013-01-09 02:31:01Z clund@localk.com $

// This version of tdFIR.c uses SSE3 instructions

#include "tdFIR.h"
#include <complex.h>
#include <x86intrin.h> // For _mm_ functions

// Process a single complex output
// Note the access pattern:
//   Calls to this function often use the entire FilterArray
//   Calls often walk the InputArray backwards by up to K elements
static inline void tdFIRwork
  ( unsigned char const *       restrict InputElement,
    unsigned char       * const restrict OutputElement,
    unsigned short                       FilterCount,
    unsigned char const *       restrict FilterElement
  )
{ float complex sum;
  __m128 sumSIMD = _mm_setzero_ps();
  unsigned short FilterDoubleCount = FilterCount / 2;
  InputElement -= sizeof(float complex);  // Primes the loop. Remember this counts backwards.
  // Start with a loop manually unrolled once
  for ( ; FilterDoubleCount != 0; FilterDoubleCount-- )
  { // Load this filter element into registers
    const __m128 FilterPair = _mm_loadu_ps ((float *)FilterElement); // loads F0, F1

    // Load this input element into registers
    __m128 InputPair = _mm_loadu_ps ((float *)InputElement); // loads I(-1), I0
    // Correct for the fact the scalar algorithm loads the elements in the opposite order
    InputPair = _mm_shuffle_ps (InputPair, InputPair, _MM_SHUFFLE(1,0,3,2)); // creates I0, I(-1)

    // Complex multiply
    sumSIMD = _mm_add_ps (sumSIMD,
      _mm_addsub_ps (
        _mm_mul_ps (
          InputPair,
    _mm_moveldup_ps ( FilterPair ) // f0real, f0real, f1real, f1real
  ), // i0r*f0r, i0i*f0r, i-1r*f1r, i-1i*f1r
        _mm_mul_ps (
          _mm_shuffle_ps (InputPair, InputPair, _MM_SHUFFLE(2,3,0,1)), // i0imag, i0rea1, i(-1)imag, i(-1)real
    _mm_movehdup_ps ( FilterPair ) // f0imag, f0imag, f1imag, f1imag
  ) // i0i*f0i, i0r*f0i, i-1i*f1i, i-1r*f1i
      ) // i0r*f0r - i0i*f0i, i0i*f0r + i0r*f0i, i-1r*f1r - i-1i*f1i, i-1i*f1r + i-1r*f1i
    );

    FilterElement += 2*sizeof(float complex);
    InputElement -= 2*sizeof(float complex);

  };

  // Next line results in {r1+r2, i1+i2, r2+r1, i2+i1)
  sumSIMD = _mm_add_ps (sumSIMD, _mm_shuffle_ps (sumSIMD, sumSIMD, _MM_SHUFFLE(1,0,3,2)));
  _mm_storel_pi((__m64 *)&sum,sumSIMD);

  // If FilterCount was odd, we need an extra, pass
  if ( (FilterCount % 2) != 0 )
  { // Load this filter element into registers
    const float FilterReal = *(float *)FilterElement;
    const float FilterImag = *(float *)(FilterElement + sizeof(float));

    // Load this input element into registers
    InputElement += sizeof(float complex);  // Unprimes the first loop.
    const float InputReal = *(float *)InputElement;
    const float InputImag = *(float *)(InputElement + sizeof(float));

    // Complex multiply
    sum += InputReal*FilterReal - InputImag*FilterImag;
    sum += I*(InputReal*FilterImag + InputImag*FilterReal);

  };

  // Store the sum
 *(float complex *)OutputElement = sum;

  return;
}

// Note in this implementation
//   K must be an even number greater than zero
//   N must be greater than K
void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{ // Variables shared by all three loops
  unsigned char       * restrict OutputElement = (unsigned char *)OutputArray;
  unsigned char const * restrict InputBase     = (unsigned char *)InputArray;

  // Process first K-1 elements differently
  for ( unsigned short FilterCount = 1; FilterCount < K; FilterCount++ )
  { tdFIRwork (InputBase, OutputElement, FilterCount, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // This is the main loop
  for ( unsigned short OutputCount = N - K; OutputCount != 0; OutputCount--)
  { tdFIRwork (InputBase, OutputElement, K, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // Process one element as above then do the last K-1 elements differently
  { unsigned char const * restrict FilterBase = (unsigned char *)FilterArray;
    for ( unsigned short FilterCount = K; FilterCount != 0; FilterCount--)
    { tdFIRwork (InputBase, OutputElement, FilterCount, FilterBase);
      OutputElement += sizeof(float complex);
      FilterBase += sizeof(float complex);
      // Note that we no longer increment InputBase
    }
  }

return;

} // End tdFIR function
