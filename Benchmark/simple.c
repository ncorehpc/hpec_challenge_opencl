// SAR Benchmark, tdFIR, scalar
// $Id: simple.c 9940 2012-12-28 18:22:29Z clund@localk.com $

// This version of tdFIR.c is the easiest to understand

#include "tdFIR.h"
#include <complex.h>

void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{ // Zero the output array
  for (int Element = 0; Element < (N+K-1); Element++)
    OutputArray[Element] = 0.0f + __extension__ 0.0fi;

  // Calculate tdFIR
  for ( int FilterElement = 0; FilterElement < K; FilterElement++ )
    for ( int InputElement = 0; InputElement < N; InputElement++ )
      OutputArray [ InputElement + FilterElement ] += InputArray [ InputElement ] * FilterArray[ FilterElement ];

  return;
}
