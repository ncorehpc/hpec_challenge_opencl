// SAR Benchmark, tdFIR, scalar
// $Id: swap.c 9940 2012-12-28 18:22:29Z clund@localk.com $

// This version of tdFIR.c inverts the loops (with respect to simple.c)

#include "tdFIR.h"
#include <complex.h>

void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{ // Process first K-1 elements differently
   
  int InputElement = 0;
  for ( ; InputElement < (K-1); InputElement++)
  { float complex sum = 0.0f;
    for (int FilterElement = 0; FilterElement <= InputElement; FilterElement++)
      sum += InputArray[InputElement - FilterElement] * FilterArray[FilterElement];
    OutputArray[InputElement] = sum;
  }

  // This is the loop to optimize as it does most of the work
  // Loop above leaves InputElement at (K-1)
  for ( ; InputElement < N; InputElement++)
  { float complex sum = 0.0f;
    for (int FilterElement = 0; FilterElement < K; FilterElement++)
      sum += InputArray[InputElement - FilterElement] * FilterArray[FilterElement];
    OutputArray[InputElement] = sum;
  }

  // Process last K-1 elements differently
  // Loop above leaves InputElement at N
  for ( ; InputElement < (N+K-1); InputElement++)
  { float complex sum = 0.0f;
    for (int FilterElement = (InputElement - N + 1); FilterElement < K; FilterElement++)
      sum += InputArray[InputElement - FilterElement] * FilterArray[FilterElement];
    OutputArray[InputElement] = sum;
  }

  return;
}
