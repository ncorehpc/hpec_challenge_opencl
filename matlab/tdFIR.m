#!/usr/bin/octave -qf
% SAR Benchmark, tdFIR, matlab
% Benchmark implemented in MATLAB
% $Id: tdFIR.m 9940 2012-12-28 18:22:29Z clund@localk.com $

dataSet = str2num(argv(){1});

inputFileName  = ['../data/' num2str(dataSet) '-tdFir-input.dat'];
filterFileName = ['../data/' num2str(dataSet) '-tdFir-filter.dat'];
resultFileName = ['../data/' num2str(dataSet) '-tdFir-output.dat'];

INPUT = readFile(inputFileName, 'float32');
filter = readFile(filterFileName, 'float32');

[rows, cols] = size (INPUT);

for yy=1:rows
  result(yy,:) = conv(INPUT(yy,:),filter(yy,:));
end

writeFile(resultFileName, result, 'float32');
