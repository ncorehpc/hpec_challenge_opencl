# Anemone examples, Lesson 1, shared
# $Id: shared.mk 10423 2013-03-28 01:34:32Z clund@localk.com $

# Make targets inside the common.mk file include:
#
# make clean    # To clean up the current directory
# make dsp      # To create tdFIR-sim.elf to run with e-gdb or e-run
# make assembly # To create tdFIR.s so you can see the code
# make host     # To create tdFIR to run on Intel host
# make run      # To run the host version without gdb
# make e-run    # To run the dsp version using e-run
# make hw       # To run the dsp version on real hardware
# make check    # To check the answer after running

# hpecChallenge LARGE dataset is N=4096, K=128, M=64
# hpecChallenge SMALL dataset is N=1024, K=12,  M=20
# One pass of the SMALL dataset fits into Anemone local memory (16,568 bytes)
# One pass of the LARGE dataset is 67,576 bytes or twice the entire local memory
# The LARGE dataset won't fit into the DRAM heap unless you dedicate all of the external DRAM to one core (LDSFLAGS default)
ifdef FITS
 MAXN = 1024
 MAXK = 12
 MAXM = 1
 DATASET = 3  # which does not exist
endif
ifdef SMALL
 MAXN = 1024
 MAXK = 12
 MAXM = 20
 DATASET = 2
endif
# default is LARGE
ifndef MAXM
 MAXN = 4096
 MAXK = 128
 MAXM = 64
 DATASET = 1
endif

# If you cannot find a file in the current directory,
# try the shared directory before giving up
VPATH = ../shared

# Turn off all default suffix processing because I'm simplistically
# mixing two architectures within this Makefile
# Thus it is a good idea to use 'clean' at the start of your make line
.SUFFIXES :

ifndef TDFIR
 TDFIR = ../Benchmark/simple.c # Original version with float complex and simple loop
endif
# TDFIR = ../Benchmark/tdFIR2.c # Contains float complex and inverted loop
# TDFIR = ../Optimize/pointers.c # Uses pointers instead of complex and array indexes
# TDFIR = ../Optimize/sse3.c # Uses Intel SIMD instructions
# TDFIR = ../Big/tdFIR4.c # Manually cache FilterArray
# TDFIR = ../Big/tdFIR5.c # Cache FilterArray and Read InputArray only once
# TDFIR = ../Big/tdFIR6.c # Cache FilterArray and DMA buffer InputArray
# TDFIR = ../Optimize/neon2.c # Neon  2-issue
# TDFIR = ../Optimize/neon4.c # Neon 4-issue

ifndef MTDFIR
 MTDFIR = ../shared/MtdFIR1.c # Uses memory layout expected by ../tdfir/main2.c
endif
# MTDFIR = ../threads/MtdFIRthreads.c # Uses threads to run multiple filters in parallel

ifndef MAIN
  MAIN = ../HPEC/tdFir.c # Without ORIGINAL defined: Timers, data on heap, uses file I/O to get/save data
endif
# MAIN = ../Start/main.c # Data on stack, zeros
# MAIN = ../shared/main.c # Timers, data on stack, zeros

ifndef LDSFLAGS
  # LDSFLAGS = -m        # Segment the DRAM among all the cores. Default is to see the entire DRAM
  # LDSFLAGS = -f        # Move stack and user code into internal memory
  # LDSFLAGS = -i        # Everything internal, no heap
  # LDSFLAGS = -s Bytes  # Size in bytes to reserve for stack. Default is 1024.
endif

ifndef CTIMER0
  CTIMER0 = CT_CLK
endif
ifndef CTIMER1
  CTIMER1 = CT_ELSTALL
endif

TARGET=$(shell echo `uname -m`)

ifeq ($(TARGET),aarch64)
include ../shared/make.aarch64
endif

ifeq ($(TARGET),riscv64)
include ../shared/make.riscv64
endif

ifeq ($(TARGET),x86_64)
include ../shared/make.x86_64
endif

ifndef DEBUG
  MYCFLAGS=$(OPT)
else
	MYCFLAGS=$(NOOPT)
endif

# Use threads
ifdef MTHREADS
  MYCFLAGS += -pthread -DMTHREADS=$(MTHREADS)
  MYENDCFLAGS = -lpthread
endif

# Use AMD's OpenCL for a GPU
ifdef AMDOPENCL
  MYCFLAGS += -I/opt/AMDAPP/include -L/opt/AMDAPP/lib/x86_64
  MYENDCFLAGS = -lOpenCL
endif

MYCFLAGS += -DMAXN=$(MAXN) -DMAXK=$(MAXK) -DMAXM=$(MAXM) -I../shared -I../../shared $(EXTRACFLAGS)
MYADSPFLAGS += -Xlinker -Map=tdFIR.map -Xlinker --cref -fstack-usage -DCTIMER0=$(CTIMER0) -DCTIMER1=$(CTIMER1)

.PHONY : all
all : dsp host assembly

.PHONY : dsp
dsp : tdFIR-hw.elf

.PHONY : intel host linux arm
intel host linux arm : tdFIR

# For the host (probably Intel or Arm)
tdFIR : $(MAIN) $(MTDFIR) $(TDFIR) tdFIR.h timeLinux.h dmaLinux.h
	$(CC) $(MYCFLAGS) $(TDFIR) $(MTDFIR) $(MAIN) -o $@ -lrt $(MYENDCFLAGS)

# Optionally generate the .s so that you can examine it
.PHONY : assembly
assembly : tdFIR.s main.s

tdFIR.s : $(TDFIR) tdFIR.h
	$(CC) -S $(MYCFLAGS) $(MYADSPFLAGS) $(MYCOREFLAGS) $< -o $@

main.s : $(MAIN) tdFIR.h
	$(CC) -S $(MYCFLAGS) $(MYADSPFLAGS) $(MYCOREFLAGS) $< -o $@

tdFIR-hw.elf : $(MAIN) $(MTDFIR) $(TDFIR)
	$(CC) $(MYCFLAGS) $(MYADSPFLAGS) $(MYCOREFLAGS) $(TDFIR) $(MTDFIR) $(MAIN) -o $@ $(LINKERFLAGS)

CLEAN += *.o *.elf *.s *.dat *.tgz *~ *.log tty ../data/*output* ../data/*time* tdFIR results.txt *.su *.lds a.out *.map
.PHONY : clean
clean :
	rm -f $(CLEAN)

../HPEC/tdFirVerify :
	$(MAKE) -C ../HPEC tdFirVerify

.PHONY : run
run: tdFIR
	rm -f ../data/$(DATASET)-tdFir-output.dat ../data/$(DATASET)-tdFir-time.dat
	./tdFIR $(DATASET)

.PHONY : check
check : ../HPEC/tdFirVerify
	../HPEC/tdFirVerify $(DATASET)

#.PHONY : e-run
#e-run : tdFIR-sim.elf
#	rm -f ../data/$(DATASET)-tdFir-output.dat ../data/$(DATASET)-tdFir-time.dat
#	e-run $<

.PHONY : hw
hw : tdFIR-hw.elf ../../shared/hw.gdb
	rm -f ../data/$(DATASET)-tdFir-output.dat ../data/$(DATASET)-tdFir-time.dat
	e-gdb -x ../../shared/hw.gdb $<

.PHONY : perf

perf : tdFIR
	../shared/run_perfstats ./tdFIR $(DATASET)

.PHONY : time
time times timer: tdFIR
	./tdFIR $(DATASET) > results.txt
	./tdFIR $(DATASET) >> results.txt
	./tdFIR $(DATASET) >> results.txt
	./tdFIR $(DATASET) >> results.txt
	./tdFIR $(DATASET) >> results.txt
	cat < results.txt
	# for old timing method
#	awk '{sum = sum + $$1 * 1000000000 + $$4} END { print "Average = ",sum/NR}' < results.txt
	awk '{sum = sum + $$3} END { print "Average = ",sum/NR}' < results.txt
	size tdFIR
	wc $(TDFIR)
