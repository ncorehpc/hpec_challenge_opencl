// Anemone Lesson 1, shared directory
// $Id: main.c 9940 2012-12-28 18:22:29Z clund@localk.com $

#include <stdlib.h> // For EXIT_SUCCESS
#include <stdio.h>   // for printf()
#include <complex.h>
#include "MtdFIR.h"
#if defined(__x86_64)
  #include "timeIntel.h"
#else
   #error "Unknown Compiler"
#endif

int main (int argc, const char *argv[]) {

// Dataset sizes are passed in using -D flags in the Makefile
// Macros used: MAXN, MAXK, MAXM
#if !defined(MAXN) || !defined(MAXK) || !defined(MAXM)
  #error "Missing -D flags on gcc command line"
#endif

// This data ends up on the program stack where it is not initialized to zero
// This allows the small dataset to fit into a core's local memory if MAXM=1
float complex InputArray [MAXN*MAXM];
float complex FilterArray[MAXK*MAXM];
float complex OutputArray[(MAXN+MAXK-1)*MAXM];

// Zero all the input data in case we run this without inserting data
// Zeroing avoids dealing with random bit patterns creating FP interrupts
// All zeros is fine when timing routines
printf("\nNo dataset used. Input all zeros allocated on program stack.\n\n");
for (int Element = 0; Element < MAXN*MAXM; Element++)
  InputArray[Element] = 0.0f + 0.0f * I;
for (int Element = 0; Element < MAXK*MAXM; Element++)
  FilterArray[Element] = 0.0f + 0.0f * I;

timeinit();
// Call tdFIR ten times
for (unsigned short c = 0; c < 10; c++)
{ timestart();
  MtdFIR ( MAXM, MAXN, MAXK, InputArray, FilterArray, OutputArray );
  timestop();
}
timeshow();
timefinish();
printf("\nFinished processing. Results not checked or saved. Exiting now.\n");

return EXIT_SUCCESS;
}
