// Anemone Lesson 1, shared directory
// Routine that calls tdFIR multiple times
// $Id: MtdFIR1.c 9940 2012-12-28 18:22:29Z clund@localk.com $

#include <complex.h> 
#include "MtdFIR.h"
#include "tdFIR.h"

void MtdFIR
  ( unsigned short  const M, // Number of filters to process
    unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputStream,  // Length N*M
    float complex   const * const restrict FilterStream, // Length K*M
    float complex * const         restrict OutputStream  // Length (N+K-1)*M
  )
{
// Call tdFIR M times
for (unsigned short m = 0; m < M; m++)
  { tdFIR ( N, K, &InputStream[m*N], &FilterStream[m*K], &OutputStream[m*(N+K-1)] );
  }

return;
}
