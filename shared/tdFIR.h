// tdFIR.h from Anemone Lesson 1, shared directory
// $Id: tdFIR.h 9940 2012-12-28 18:22:29Z clund@localk.com $
#ifndef TDFIR_H
#define TDFIR_H
#include <complex.h>
extern void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )  __attribute__((nothrow,hot,nonnull));
#endif // TDFIR_H
