// tdFIR.h from Anemone Lesson 1, shared directory
// $Id: MtdFIR.h 9940 2012-12-28 18:22:29Z clund@localk.com $
#ifndef MTDFIR_H
#define MTDFIR_H
#include <complex.h>
extern void MtdFIR
  ( unsigned short  const M, // Number of filters to process
    unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N*M
    float complex   const * const restrict FilterArray, // Length K*M
    float complex * const         restrict OutputArray  // Length (N+K-1)*M
  );
#endif // MTDFIR_H
