/* $Id: timeLinux.h 10399 2013-03-26 17:09:09Z clund@localk.com $ */
#ifndef TIMELINUX_H
#define TIMELINUX_H

#include <time.h>   // For clock_gettime()
#include <stdio.h>  // For printf()

#ifndef PICKCLOCK
  #define PICKCLOCK CLOCK_MONOTONIC_RAW
#endif
// CLOCK_REALTIME
// CLOCK_MONOTONIC
// CLOCK_PROCESS_CPUTIME_ID
// CLOCK_THREAD_CPUTIME_ID

#define BILLION 1000000000L

/**
 *  Gets the time in seconds given a struct timespec
 *
 * @return  double of the time in seconds of the arg
 */
static __attribute__((always_inline)) inline double  time_seconds(struct timespec time)
{
    return (double)time.tv_sec + (double)time.tv_nsec / (double)BILLION;
}

/**
 * Returns the difference of two struct timespec args
 *
 * @param old old time
 * @param new new time
 *
 * @return  new - old returned as a struct timespec
 */
static inline struct timespec diff_time(struct timespec old, struct timespec new)
{
    if (new.tv_nsec < old.tv_nsec)
        return (struct timespec)
    {
        .tv_sec = new.tv_sec - 1 - old.tv_sec,
        .tv_nsec = BILLION + new.tv_nsec - old.tv_nsec
    };
    else
        return (struct timespec)
    {
        .tv_sec = new.tv_sec - old.tv_sec,
        .tv_nsec = new.tv_nsec - old.tv_nsec
    };
}

static struct timespec timediff (
  struct timespec tstart,
  struct timespec tend
  )
{ struct timespec telapsed;
  // Check if nanoseconds needs a carry
  if ((tend.tv_nsec - tstart.tv_nsec) < 0 )
  { if (tend.tv_sec == 0)
   // This can happen when you time small things
   { telapsed.tv_sec = 0;
     telapsed.tv_nsec = 0;
   } else
   // Carry one second over from seconds
   { telapsed.tv_sec = tend.tv_sec - tstart.tv_sec - 1;
     telapsed.tv_nsec = ( 1000000000 + tend.tv_nsec - tstart.tv_nsec );
   }
  } else
  // No carry is necessary
  { telapsed.tv_sec = tend.tv_sec - tstart.tv_sec;
    telapsed.tv_nsec = tend.tv_nsec - tstart.tv_nsec;
  }
  return telapsed;
}

static struct timespec tstart, tend, telapsed, toverh;

static inline void timestart (void)
{
  if ( clock_gettime(PICKCLOCK, &tstart) != 0 )
  { perror("timestart"); exit(1);};
  return;
}

static inline void timestop (void)
{ if ( clock_gettime(PICKCLOCK, &tend) != 0 )
  { perror("timestop"); exit(1);};
  // old method of printing times
  // telapsed = timediff(tstart, tend);
  // telapsed = timediff(toverh, telapsed);
  telapsed = diff_time(tstart, tend);
  telapsed = diff_time(toverh, telapsed);
  return;
}

static void timeshow (void)
{
 // old method of printing times
 // printf ("%lu seconds and %lu nanoseconds.\n", telapsed.tv_sec, telapsed.tv_nsec);
    printf("Loop Seconds: %8.9f\n",time_seconds(telapsed));
  return;
}

static void timeinit (void)
{
  toverh.tv_sec = 0;
  toverh.tv_nsec = 0;
  timestart();
  timestop();
  toverh = telapsed;

  return;
}

// Does nothing on Linux
static void timefinish (void)
{ return;
}

#endif // TIMELINUX_H
