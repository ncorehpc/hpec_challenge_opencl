// Anemone Lesson 1, shared directory
// $Id: dmaLinux.h 9940 2012-12-28 18:22:29Z clund@localk.com $
#ifndef DMAINTEL_H
#define DMAINTEL_H

#include <string.h> // For memcpy

// Using memcpy instead of DMA means only one channel
#define dma1Start dma0Start
#define dma1Wait dma0Wait
#define dma1Init dma0Init

// Does nothing (no DMA engine)
static void dma0Init (void)
{ return;
}

// Does nothing (no DMA engine)
inline static void dma0Finish (void)
{ return;
}

// Length is number of double words
// memcpy might be faster but I'm using this for overlapping buffers
static void inline dma0Start (char *dmaDestination, char *dmaSource, unsigned int dmaLength)
{ (void) memmove (dmaDestination, dmaSource, dmaLength*sizeof(double));
  return;
}

// Does nothing because dma0Start() is blocking on Intel (no DMA engine)
inline static void dma0Wait (void)
{ return;
}

#endif // DMAINTEL_H
