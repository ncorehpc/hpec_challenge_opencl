# HPEC tdFIR Benchmark for NEON and OpenCL #

This package implements the tdFIR benchmark from the HPEC Benchmark Suite for:

* OpenCL
* Arm NEON
* OpenMP
* Other Optimizations

### Running Benchmarks ###

The first step is to benchmark the chip using the basic form of the loop:

`cd Benchmark`

`make clean host`

`make run check # To verify the benchmark is generating the correct answers`

`make time # To run it a few time and get an answer`

`make clean host TDFIR=swap.c`

``make run check`

`make time`

#### ARM Neon ####

Now let us try Neon
This chip as 2-wide Neon inside which simulates 4-wide
`neon4.c` is also available

`cd ../Optimize`

``make clean host TDFIR=neon2.c NEON=yes`

`make run check`

`make time`

#### OpenMP and swap.c ####

`cd ../OpenMP`

`make clean host MTDFIR=MtdFIROMP.c TDFIR=../Benchmark/swap.c`

`make run check`

`make time`

#### Bandwidth Calculation ####

Last for this demo I'm going to test where the chip becomes memory limited
his is done by copying data instead of calculating answers

`cd ../Optimize``

``make clean host TDFIR=bandwidth.c`

**You cannot "Make check" because the answers are known wrong**

`make time`

### Performance Counter Measurements ###

`make perf`

Runs the built benchmark and measures counters.  The type and variety can be
selected by editing the `./shared/perfstat.<arch> ` file for the `<arch>` in question
