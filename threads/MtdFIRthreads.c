// SAR Benchmark, tdFIR, threads
// Routine that calls tdFIR multiple times using threads
// $Id: MtdFIRthreads.c 7 2012-10-27 00:16:06Z clund $

#include <complex.h> 
#include "MtdFIR.h"
#include "tdFIR.h"
#include <pthread.h>
#include <error.h>
#include <stdlib.h>

struct tdFIRparams
  { unsigned short                       M; // Number of filters to process
    unsigned short                       N; // Size of input in number of complex elements
    unsigned short                       K; // Size of filter in number of complex elements
    float complex const * restrict InputStream;  // Length N*M
    float complex const * restrict FilterStream; // Length K*M
    float complex       * restrict OutputStream;  // Length (N+K-1)*M
  };

void *thruThread(void *params)
#define myparams ((struct tdFIRparams *)params)
{
  for (unsigned short m = 0; m < myparams->M; m++)
    tdFIR ( myparams->N, myparams->K, &myparams->InputStream[m*myparams->N], &myparams->FilterStream[m*myparams->K], &myparams->OutputStream[m*(myparams->N+myparams->K-1)] );
  pthread_exit(NULL);
}

void MtdFIR
  ( unsigned short  const                  M, // Number of filters to process
    unsigned short  const                  N, // Size of input in number of complex elements
    unsigned short  const                  K, // Size of filter in number of complex elements
    float complex   const * const restrict InputStream,  // Length N*M
    float complex   const * const restrict FilterStream, // Length K*M
    float complex         * const restrict OutputStream  // Length (N+K-1)*M
  )
{
  pthread_t             thread[MTHREADS];
  pthread_attr_t 	attr;
  struct tdFIRparams	params[MTHREADS];
  int rc;

  if (MTHREADS <=0 || MTHREADS > M)
    error (EXIT_FAILURE, 0, "MTHREADS probably too high for M\n");
  
  // Initialize and set thread joinable attribute
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  
  // Calculate the number of filters given to each thread
  unsigned short threadM = M / MTHREADS;
  
  unsigned short i;
  for ( i=0; i < MTHREADS; i++)
  {  params[i].M = threadM;
     params[i].N = N;
     params[i].K = K;
     params[i].InputStream = &InputStream[i*threadM*N];
     params[i].FilterStream = &FilterStream[i*threadM*K];
     params[i].OutputStream = &OutputStream[i*threadM*(N+K-1)];
     if ( rc = pthread_create(&thread[i], &attr, thruThread, (void *)&params[i]) )
       error (EXIT_FAILURE, rc, "pthread_create()\n");
   }

  // This thread will do any extra work required if extraM !=0
  unsigned short extraM = M % MTHREADS;
  for (unsigned short m = 0; m < extraM; m++)
    tdFIR ( N, K, &(&InputStream[i*threadM*N])[m*N], &(&FilterStream[i*threadM*K])[m*K], &(&OutputStream[i*threadM*(N+K-1)])[m*(N+K-1)] );
  
   // Free attribute Kand wait for all the threads to finish
   pthread_attr_destroy(&attr);
   for ( unsigned short i=0; i < MTHREADS; i++)
     if ( rc = pthread_join(thread[i], NULL) )
       error (EXIT_FAILURE, rc, "pthread_join()\n");

return;
}
