// SAR Benchmark, tdFIR. threads
// $Id: sse3threads.c 13 2012-12-11 21:06:25Z clund $

// This version of tdFIR.c uses SSE3 instructions
// It also uses pthreads

#include "tdFIR.h"
#include <complex.h>
#include <x86intrin.h> // For _mm_ functions
#include <pthread.h>
#include <error.h>
#include <stdlib.h>

// Process a single complex output
// Note the access pattern:
//   Calls to this function often use the entire FilterArray
//   Calls often walk the InputArray backwards by up to K elements
static inline void tdFIRwork
  ( char const *       restrict InputElement,
    char       * const restrict OutputElement,
    unsigned     short          FilterCount,
    char const *       restrict FilterElement
  )
{ float complex sum;
  __m128 sumSIMD = _mm_setzero_ps();
  unsigned short FilterDoubleCount = FilterCount / 2;
  // Start with a loop manually unrolled once
  for ( ; FilterDoubleCount != 0; FilterDoubleCount-- )
  { // Load this filter element into registers
    const __m128 FilterPair = _mm_loadu_ps ((float *)FilterElement);

    // Load this input element into registers
    __m128 InputPair = _mm_loadu_ps ((float *)(InputElement - sizeof(float complex)));
    // Correct for the fact the scalar algorithm loads the elements in the opposite order
    InputPair = _mm_shuffle_ps (InputPair, InputPair, _MM_SHUFFLE(1,0,3,2));
    
    // Complex multiply
    sumSIMD = _mm_add_ps (sumSIMD, _mm_addsub_ps ( _mm_mul_ps ( InputPair, _mm_moveldup_ps ( FilterPair ) ), _mm_mul_ps ( _mm_shuffle_ps (InputPair, InputPair, _MM_SHUFFLE(2,3,0,1)), _mm_movehdup_ps ( FilterPair ) ) ));

    FilterElement += 2*sizeof(float complex);
    InputElement -= 2*sizeof(float complex);

  };
  
  // Next line results in {r1+r2, i1+i2, r2+r1, i2+i1)
  sumSIMD = _mm_add_ps (sumSIMD, _mm_shuffle_ps (sumSIMD, sumSIMD, _MM_SHUFFLE(1,0,3,2)));
  _mm_storel_pi((__m64 *)&sum,sumSIMD);
   
  // If FilterCount was odd, we need an extra, pass
  if ( (FilterCount % 2) != 0 )
  { // Load this filter element into registers
    const float FilterReal = *(float *)FilterElement;
    const float FilterImag = *(float *)(FilterElement + sizeof(float));

    // Load this input element into registers       
    const float InputReal = *(float *)InputElement;
    const float InputImag = *(float *)(InputElement + sizeof(float));

    // Complex multiply
    sum += InputReal*FilterReal - InputImag*FilterImag;
    sum += I*(InputReal*FilterImag + InputImag*FilterReal);

  };

  // Store the sum
 *(float complex *)OutputElement = sum;

  return;
}


struct tdFIRworkparams
  { unsigned short		 OutputCount;
    char const *       restrict InputBase;
    char       *       restrict OutputElement;
    unsigned     short          K;
    char const *       restrict FilterArray;
  };

void *turnThread(void *params)
#define myparams ((struct tdFIRworkparams *)params)
{ char * restrict OutputElement = myparams->OutputElement;
  char const * restrict InputBase = myparams->InputBase;
  unsigned short OutputCount = myparams->OutputCount;
  do 
    { tdFIRwork (InputBase, OutputElement, myparams->K, myparams->FilterArray);
      InputBase += sizeof(float complex);
      OutputElement += sizeof(float complex);
      OutputCount--;
    } while (OutputCount!=0);
  pthread_exit(NULL);
}


// Note in this implementation
//   K must be an even number greater than zero
//   N must be greater than K
void tdFIR
  ( unsigned short  const N, // Size of input in number of complex elements
    unsigned short  const K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N
    float complex   const * const restrict FilterArray, // Length K
    float complex * const         restrict OutputArray  // Length N+K-1
  )
{ // Variables shared by all three loops
  char * restrict OutputElement = (char *)OutputArray;
  char * restrict InputBase = (char *)InputArray;
  
    
  // Process first K-1 elements differently
  for ( unsigned short FilterCount = 1; FilterCount < K; FilterCount++ )
  { tdFIRwork (InputBase, OutputElement, FilterCount, (unsigned char *)FilterArray);
    InputBase += sizeof(float complex);
    OutputElement += sizeof(float complex);
  }

  // This is the main loop
  { unsigned short OutputCount = N - K;
  
    pthread_t            	thread[MTHREADS];
    pthread_attr_t		attr;
    struct tdFIRworkparams	params[MTHREADS];
    int rc;

    // Initialize and set thread joinable attribute
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  
    if (MTHREADS <=0 || MTHREADS > OutputCount)
      error (EXIT_FAILURE, 0, "MTHREADS probably too high for size of InputArray\n"); 
  
    unsigned short const OutputChunk = OutputCount / MTHREADS;
  
    for (unsigned short i=0; i < MTHREADS; i++)
    {  params[i].OutputCount = OutputChunk;
       params[i].InputBase = InputBase;
       params[i].OutputElement = OutputElement;
       params[i].K = K;
       params[i].FilterArray = (char *)FilterArray;
       if ( rc = pthread_create(&thread[i], &attr, turnThread, (void *)&params[i]) )
         error (EXIT_FAILURE, rc, "pthread_create()\n");
       InputBase += sizeof(float complex) * OutputChunk;
       OutputElement += sizeof(float complex) * OutputChunk;
       OutputCount-=OutputChunk;
     };
   
     for ( ; OutputCount > 0; OutputCount--)
     { tdFIRwork (InputBase, OutputElement, K, (char *)FilterArray);
       InputBase += sizeof(float complex);
       OutputElement += sizeof(float complex);
      };

    // Free attribute and wait for all the threads to finish
     pthread_attr_destroy(&attr);
     for ( unsigned short i=0; i < MTHREADS; i++)
       if ( rc = pthread_join(thread[i], NULL) )
         error (EXIT_FAILURE, rc, "pthread_join()\n");
       
  } // End main loop isolation block

  // Process one element as above then do the last K-1 elements differently
  { unsigned char const * restrict FilterBase = (unsigned char *)FilterArray;
    for ( unsigned short FilterCount = K; FilterCount != 0; FilterCount--)
    { tdFIRwork (InputBase, OutputElement, FilterCount, FilterBase);
      OutputElement += sizeof(float complex);
      FilterBase += sizeof(float complex);
      // Note that we no longer increment InputBase
    }
  }

return;

} // End tdFIR function
