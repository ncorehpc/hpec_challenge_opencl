// OpenCL static kernel language implementation of tdFIR for a GPU 
// $Id: simplenull.cl 9969 2013-01-09 16:51:16Z clund@localk.com $

// Zero the output buffer
__kernel void tdFIRworkFirst
  ( __global float2 * const restrict OutputArray  // Length (N+K-1)*M
  )
{ return;
} // End tdFIRworkFirst


// Calculate the tdFIR
__kernel void tdFIRworkMain
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N*M
    __constant          float2    const * const restrict FilterArray, // Length K*M which fits into my GPU's constant memory
    __global   volatile float2  * const         restrict OutputArray  // Length (N+K-1)*M
  )
{ return;
} // End tdFIRworkMain
