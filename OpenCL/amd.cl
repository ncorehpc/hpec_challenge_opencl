// SAR Benchmark, tdFIR, OpenCL
// OpenCL static kernel language implementation of tdFIR for a GPU 
// $Id: amd.cl 9970 2013-01-09 20:00:52Z clund@localk.com $

// Multiply two complex numbers
__private inline float2 cmult
  ( __private float2 a, 
    __private float2 b
  )
{
  return (__private float2)( a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

static inline void tdFIRwork
  ( __global            float2    const * const restrict InputSubArray,   
    __global            float2  * const         restrict OutputElement,
    __private  unsigned short     const                  FilterCount,  // Number of filter elements to process
    __constant          float2    const * const restrict FilterSubArray  
  )
{ 
  __private  float2                        sum           = {0.0f, 0.0f};
  __global   float2 const *       restrict InputElement  = InputSubArray;
  __constant float2 const *       restrict FilterElement = FilterSubArray;

    
  for ( __private unsigned short Count = FilterCount; Count !=0 ; Count-- )
  { sum += cmult( *InputElement, *FilterElement );
    FilterElement += 1;
    InputElement  -= 1;
  };

  // Store the sum
  *OutputElement = sum;
  
  return;
 
}

__kernel void tdFIRworkMain
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N
    __constant          float2    const * const restrict FilterArray, // Length K which fits into my GPU's constant memory
    __global            float2  * const         restrict OutputArray  // Length N+K-1
  )
{ 
  __private size_t m           = get_global_id (0);
  __private size_t OutputCount = get_global_id (1);
  
  if (OutputCount >= N) // Allows padding when establishing local work groups
    return;

  __global            float2 const *       restrict InputSubArray  = InputArray  + m*N + OutputCount;
  __constant          float2 const *       restrict FilterSubArray = FilterArray + m*K;
  __global            float2       * const restrict OutputElement  = OutputArray + m*(N+K-1) + OutputCount;
      
  tdFIRwork(InputSubArray, OutputElement, K, FilterSubArray);
      
  return;
 
} // End tdFIRworkMain

__kernel void tdFIRworkFirst
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N
    __constant          float2    const * const restrict FilterArray, // Length K which fits into my GPU's constant memory
    __global            float2  * const         restrict OutputArray  // Length N+K-1
  )
{ 
  __private size_t m           = get_global_id (0);
  __private size_t FilterCount = get_global_id (1);

  if (FilterCount >= K) // Allows padding when establishing local work groups
    return; 

  __global            float2 const *       restrict InputSubArray  = InputArray  + m*N + (FilterCount - 1 );
  __constant          float2 const *       restrict FilterSubArray = FilterArray + m*K;
  __global            float2       * const restrict OutputElement  = OutputArray + m*(N+K-1) + (FilterCount - 1 );
      
  tdFIRwork(InputSubArray, OutputElement, FilterCount, FilterSubArray);
      
  return;
 
} // End tdFIRworkFirst

__kernel void tdFIRworkLast
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N
    __constant          float2    const * const restrict FilterArray, // Length K which fits into my GPU's constant memory
    __global            float2  * const         restrict OutputArray  // Length N+K-1
  )
{ 
  __private size_t m           = get_global_id (0);
  __private size_t FilterCount = get_global_id (1);

  if (FilterCount >= K) // Allows padding when establishing local work groups
    return; 

  __global            float2 const *       restrict InputSubArray  = InputArray  + m*N + N - 1;
  __constant          float2 const *       restrict FilterSubArray = FilterArray + m*K + (K -FilterCount);
  __global            float2       * const restrict OutputElement  = OutputArray + m*(N+K-1) + (K -FilterCount) + N - 1;
      
  tdFIRwork(InputSubArray, OutputElement, FilterCount, FilterSubArray);
      
  return;
 
} // End tdFIRworkLast