// SAR Benchmark, tdFIR, OpenCL
// Routine that calls tdFIR multiple times using OpenCL
// $Id: MtdFIRsimple.c 9970 2013-01-09 20:00:52Z clund@localk.com $

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <error.h>
#include <errno.h>
#include <complex.h>
#include "MtdFIR.h"

#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS 1

#include <CL/cl.h>

#define CL_CHECK(_expr)                                                         \
   do {                                                                         \
     cl_int _err = _expr;                                                       \
     if (_err == CL_SUCCESS)                                                    \
       break;                                                                   \
     fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err);   \
     abort();                                                                   \
   } while (0)

#define CL_CHECK_ERR(_expr)                                                     \
   ({                                                                           \
     cl_int _err = CL_INVALID_VALUE;                                            \
     typeof(_expr) _ret = _expr;                                                \
     if (_err != CL_SUCCESS) {                                                  \
       fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err); \
       abort();                                                                 \
     }                                                                          \
     _ret;                                                                      \
   })

void MtdFIR
  ( unsigned short  const                  M, // Number of filters to process
    unsigned short  const                  N, // Size of input in number of complex elements
    unsigned short  const                  K, // Size of filter in number of complex elements
    float complex   const * const restrict InputArray,  // Length N*M
    float complex   const * const restrict FilterArray, // Length K*M
    float complex         * const restrict OutputArray  // Length (N+K-1)*M
  )
{
    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_program program;

    FILE* programHandle; char *programBuffer; char *programLog;
    size_t programSize;

    // get first available sdk and cpu and create context
    CL_CHECK( clGetPlatformIDs(1, &platform, NULL) );
    CL_CHECK( clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL) );
    context = CL_CHECK_ERR( clCreateContext(NULL, 1, &device, NULL, NULL, &_err) );

   // Read kernel binary into a buffer
    { struct stat instat;
      FILE *programHandle;
      programHandle = fopen("simple.bin", "r");
      if (programHandle == NULL)
      { perror ("Error opening compiled OpenCL program binary file");
        abort();
      }
      if (fstat(fileno(programHandle), &instat) != 0)
        error (EXIT_FAILURE, errno, "Error doing an fstat() on the input file");
      programSize = instat.st_size;

      // read kernel source into buffer
      programBuffer = (char*) malloc(programSize);
      { size_t result = fread(programBuffer, sizeof(char), programSize, programHandle);
        if (result != programSize)
        { perror ("Error reading compiled OpenCL program binary file");
          abort();
        }
      }
      fclose(programHandle);
    }

    // create and build program
    cl_int binaryStatus;
    program = CL_CHECK_ERR( clCreateProgramWithBinary(context, 1, &device, &programSize, (const unsigned char **) &programBuffer, &binaryStatus, &_err) );
    if (binaryStatus != CL_SUCCESS)
    {
       fprintf(stderr, "OpenCL Error: clCreateProgramWithBinary() did not return success\n");
       abort();
    }
    free(programBuffer);

    cl_int build_error;
    build_error = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
    if (build_error != CL_SUCCESS) {
	size_t logSize;
	cl_build_status status;

        // check build error and build status first
        CL_CHECK( clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &status, NULL) );

        // check build log
        CL_CHECK( clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize) );
        programLog = (char*) calloc (logSize+1, sizeof(char));
        CL_CHECK( clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, logSize+1, programLog, NULL) );
        printf("Build failed; error=%d, status=%d, programLog:\n\n%s", build_error, status, programLog);
        free(programLog);
	abort();
    }

    // create kernels and command queue
    cl_kernel         kernelFirst = CL_CHECK_ERR( clCreateKernel(program, "tdFIRworkFirst", &_err) );
    cl_kernel         kernelMain  = CL_CHECK_ERR( clCreateKernel(program, "tdFIRworkMain", &_err) );
    cl_command_queue  queue       = CL_CHECK_ERR( clCreateCommandQueue(context, device, 0, &_err) );

    // Create buffers used to communicate with the GPU
    cl_mem InputBuffer = CL_CHECK_ERR( clCreateBuffer(context,
      CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, M*N*sizeof(float complex), NULL, &_err) );

    cl_mem FilterBuffer = CL_CHECK_ERR( clCreateBuffer(context,
      CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, M*K*sizeof(float complex), NULL, &_err) );

    cl_mem OutputBuffer = CL_CHECK_ERR( clCreateBuffer(context,
      CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, M*(N+K-1)*sizeof(float complex), NULL, &_err) );

    // Write the data into the GPU
    CL_CHECK( clEnqueueWriteBuffer(queue, InputBuffer,  CL_FALSE, 0, M*N*sizeof(float complex), (void *)InputArray, 0, NULL, NULL) );
    CL_CHECK( clEnqueueWriteBuffer(queue, FilterBuffer, CL_FALSE, 0, M*K*sizeof(float complex), (void *)FilterArray, 0, NULL, NULL) );

    // Execute First kernel
    CL_CHECK( clSetKernelArg(kernelFirst, 0, sizeof(OutputBuffer), &OutputBuffer) );
    size_t global_work_size1[]   = { M*(N+K-1) };
    CL_CHECK( clEnqueueNDRangeKernel(queue, kernelFirst, 1, NULL, global_work_size1, NULL, 0, NULL, NULL) );

    // Execute Main kernel
    CL_CHECK( clSetKernelArg(kernelMain, 0, sizeof(N), (const void *)&N) );
    CL_CHECK( clSetKernelArg(kernelMain, 1, sizeof(K), (const void *)&K) );
    CL_CHECK( clSetKernelArg(kernelMain, 2, sizeof(InputBuffer), &InputBuffer ) );
    CL_CHECK( clSetKernelArg(kernelMain, 3, sizeof(FilterBuffer), &FilterBuffer) );
    CL_CHECK( clSetKernelArg(kernelMain, 4, sizeof(OutputBuffer), &OutputBuffer) );
    size_t global_work_size2[]   = { M, N, K };
    size_t local_work_size2[]   = { 1, 1, K }; // The GPU I'm using allows 256 work items. The K dataset is usually 128.
    CL_CHECK( clEnqueueNDRangeKernel(queue, kernelMain, 3, NULL, global_work_size2, local_work_size2, 0, NULL, NULL) );

    // Pull the answers out of the GPU
    CL_CHECK( clEnqueueReadBuffer(queue, OutputBuffer, CL_TRUE, 0, M*(N+K-1)*sizeof(float complex), (void *)OutputArray, 0, NULL, NULL) );

    CL_CHECK( clFlush (queue) );
    CL_CHECK( clFinish(queue) );
    CL_CHECK( clReleaseMemObject(InputBuffer) );
    CL_CHECK( clReleaseMemObject(FilterBuffer) );
    CL_CHECK( clReleaseMemObject(OutputBuffer) );
    CL_CHECK( clReleaseKernel(kernelFirst) );
    CL_CHECK( clReleaseKernel(kernelMain) );
    CL_CHECK( clReleaseProgram(program) );
    CL_CHECK( clReleaseCommandQueue(queue) );
    CL_CHECK( clReleaseContext(context) );
    CL_CHECK( clReleaseDevice(device) );

  return;

} // End MtdFIR function
