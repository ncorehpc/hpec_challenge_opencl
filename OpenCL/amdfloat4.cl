// SAR Benchmark, tdFIR, OpenCL
// OpenCL static kernel language implementation of tdFIR for a GPU
// $Id: amdfloat4.cl 9970 2013-01-09 20:00:52Z clund@localk.com $

// Multiply two complex numbers
__private inline float2 cmult
(__private float2 a,
 __private float2 b
)
{
    return (__private float2)(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

static inline void tdFIRwork
(__global            float2    const *const restrict InputSubArray,
 __global            float2   *const         restrict OutputElement,
 __private  unsigned short     const                  FilterCount,  // Number of filter elements to process
 __constant          float2    const *const restrict FilterSubArray
)
{
    // The next 2 declarations allow us to modify the pointers
    __global   float2 const        *restrict InputElement  = InputSubArray;
    __constant float2 const        *restrict FilterElement = FilterSubArray;
    __private float4 sumSIMD = {0.0f, 0.0f, 0.0f, 0.0f};
    __private unsigned short FilterDoubleCount = FilterCount / 2;
    InputElement -= 1; // Primes the loop. Remember this counts backwards.

    // Start with a loop manually unrolled once
    // I don't know AMD's SIMD capabilities; using Intel SSE3 structure
    for (; FilterDoubleCount != 0; FilterDoubleCount--)
    {
        // Load this filter element into registers
        __private float4 const FilterPair = vload4(0, (__constant float *)FilterElement);   // loads f0, f1
        __private float4 InputPair = *(__global float4 const *)InputElement;   // loads i(-1), i(0)
        InputPair = shuffle(InputPair, (uint4)(2, 3, 0, 1)); // becomes i(0), i(-1)
        __private float4 const temp1 =
            InputPair
            * shuffle(FilterPair, (uint4)(0, 0, 2, 2));
        __private float4 const temp2 =
            shuffle(InputPair, (uint4)(1, 0, 3, 2))
            * shuffle(FilterPair, (uint4)(1, 1, 3, 3));
        sumSIMD += select(temp1 - temp2, temp1 + temp2, (int4)(0, -1, 0, -1));
        FilterElement += 2;
        InputElement  -= 2;
    }

    __private float2 sum = sumSIMD.xy + sumSIMD.zw;

    // If FilterCount was odd, we need an extra, pass
    if ((FilterCount % 2) != 0)
    {
        InputElement += 1;  // Unprimes the first loop.
        sum += cmult(*InputElement, *FilterElement);
    };

    // Store the sum
    *OutputElement = sum;

    return;
}

__kernel void tdFIRworkMain
(__private  unsigned short     const                  N,            // Size of input in number of complex elements
 __private  unsigned short     const                  K,           // Size of filter in number of complex elements
 __global            float2    const *const restrict InputArray,   // Length N
 __constant          float2    const *const restrict FilterArray,  // Length K which fits into my GPU's constant memory
 __global            float2   *const         restrict OutputArray  // Length N+K-1
)
{
    __private size_t m           = get_global_id(0);
    __private size_t OutputCount = get_global_id(1);

    if (OutputCount >= N) // Allows padding when establishing local work groups
        return;

    __global            float2 const        *restrict InputSubArray  = InputArray  + m * N + OutputCount;
    __constant          float2 const        *restrict FilterSubArray = FilterArray + m * K;
    __global            float2        *const restrict OutputElement  = OutputArray + m * (N + K - 1) + OutputCount;
    tdFIRwork(InputSubArray, OutputElement, K, FilterSubArray);
    return;
} // End tdFIRworkMain

__kernel void tdFIRworkFirst
(__private  unsigned short     const                  N,            // Size of input in number of complex elements
 __private  unsigned short     const                  K,           // Size of filter in number of complex elements
 __global            float2    const *const restrict InputArray,   // Length N
 __constant          float2    const *const restrict FilterArray,  // Length K which fits into my GPU's constant memory
 __global            float2   *const         restrict OutputArray  // Length N+K-1
)
{
    __private size_t m           = get_global_id(0);
    __private size_t FilterCount = get_global_id(1);

    if (FilterCount >= K) // Allows padding when establishing local work groups
        return;

    __global            float2 const        *restrict InputSubArray  = InputArray  + m * N + (FilterCount - 1);
    __constant          float2 const        *restrict FilterSubArray = FilterArray + m * K;
    __global            float2        *const restrict OutputElement  = OutputArray + m * (N + K - 1) + (FilterCount - 1);
    tdFIRwork(InputSubArray, OutputElement, FilterCount, FilterSubArray);
    return;
} // End tdFIRworkFirst

__kernel void tdFIRworkLast
(__private  unsigned short     const                  N,            // Size of input in number of complex elements
 __private  unsigned short     const                  K,           // Size of filter in number of complex elements
 __global            float2    const *const restrict InputArray,   // Length N
 __constant          float2    const *const restrict FilterArray,  // Length K which fits into my GPU's constant memory
 __global            float2   *const         restrict OutputArray  // Length N+K-1
)
{
    __private size_t m           = get_global_id(0);
    __private size_t FilterCount = get_global_id(1);

    if (FilterCount >= K) // Allows padding when establishing local work groups
        return;

    __global            float2 const        *restrict InputSubArray  = InputArray  + m * N + N - 1;
    __constant          float2 const        *restrict FilterSubArray = FilterArray + m * K + (K - FilterCount);
    __global            float2        *const restrict OutputElement  = OutputArray + m * (N + K - 1) + (K - FilterCount) + N - 1;
    tdFIRwork(InputSubArray, OutputElement, FilterCount, FilterSubArray);
    return;
} // End tdFIRworkLast
