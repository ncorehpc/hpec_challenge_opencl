// OpenCL static kernel language implementation of tdFIR for a GPU
// $Id: simple.cl 9969 2013-01-09 16:51:16Z clund@localk.com $

// Multiply two complex numbers
__private inline float2 cmult
(__private float2 a,
 __private float2 b
)
{
    return (__private float2)(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

// Zero the output buffer
__kernel void tdFIRworkFirst
(__global float2 *const restrict OutputArray    // Length (N+K-1)*M
)
{
    __private size_t MElement = get_global_id(0);
    OutputArray[MElement] = (__private float2)(0.0f, 0.0f);
    return;
} // End tdFIRworkFirst


// Safely add a value to a global sum
inline void addbump
(__global volatile float *const restrict sumptr,
 __private float const bump
)
{
    __private float newVal, prevVal;

    do
    {
        prevVal = *sumptr;
        newVal = prevVal + bump;
    }
    while (atomic_cmpxchg((volatile __global uint *)sumptr, *(uint *)(&prevVal), *(uint *)(&newVal)) != *(uint *)(&prevVal));

    return;
}

// Calculate the tdFIRwork
// In the example that I use for benchmarking M=64, N=4096, K=128
// That translates into InputBuffer=2M, FilterArray=64k, OutputBuffer=2.1M
// The AMD GPU I'm targeting has global=512M, local=32K, constant=64k

__kernel void tdFIRworkMain
(__private  unsigned short     const                  N,            // Size of input in number of complex elements
 __private  unsigned short     const                  K,           // Size of filter in number of complex elements
 __global            float2    const *const restrict InputArray,   // Length N*M elements
 __constant          float2    const *const restrict FilterArray,  // Length K*M which fits into my GPU's constant memory
 __global   volatile float2   *const         restrict OutputArray  // Length (N+K-1)*M elements
)
{
    __private size_t MElement      = get_global_id(0);
    __private size_t InputElement  = get_global_id(1);
    __private size_t FilterElement = get_global_id(2);
#define OutputBase  MElement*(N+K-1)
#define InputBase   MElement*N
#define FilterBase  MElement*K
    __private float2 bump = cmult(InputArray[InputBase + InputElement], FilterArray[FilterBase + FilterElement]);
    // Taking the address of a vector element is not legal
    __global volatile float *const restrict sumptrx = (__global float *) & (OutputArray[OutputBase + InputElement + FilterElement]);
    __global volatile float *const restrict sumptry = sumptrx + 1;
    addbump(sumptrx, bump.x);
    addbump(sumptry, bump.y);
    return;
} // End tdFIRworkMain
