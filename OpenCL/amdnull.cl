// SAR Benchmark, tdFIR, OpenCL
// OpenCL static kernel language implementation of tdFIR for a GPU 
// $Id: amdnull.cl 9969 2013-01-09 16:51:16Z clund@localk.com $

__kernel void tdFIRworkMain
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N
    __constant          float2    const * const restrict FilterArray, // Length K which fits into my GPU's constant memory
    __global            float2  * const         restrict OutputArray  // Length N+K-1
  )
{ return;
} // End tdFIRworkMain

__kernel void tdFIRworkFirst
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N
    __constant          float2    const * const restrict FilterArray, // Length K which fits into my GPU's constant memory
    __global            float2  * const         restrict OutputArray  // Length N+K-1
  )
{ return; 
} // End tdFIRworkFirst

__kernel void tdFIRworkLast
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N
    __constant          float2    const * const restrict FilterArray, // Length K which fits into my GPU's constant memory
    __global            float2  * const         restrict OutputArray  // Length N+K-1
  )
{ return;
} // End tdFIRworkLast