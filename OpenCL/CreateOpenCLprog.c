// SAR Benchmark, tdFIR, OpenCL
// Routine that calls tdFIR multiple times using OpenCL
// $Id: CreateOpenCLprog.c 9969 2013-01-09 16:51:16Z clund@localk.com $

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <error.h>
#include <errno.h>
#include <complex.h>
#include "MtdFIR.h"

#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS 1

#include <CL/cl.h>

// GNU style command line option parsing
// Documentation http://argtable.sourceforge.net
#include <argtable2.h>

extern char *program_invocation_name;

// You find the following CHECK macros in a number of OpenCL examples
// I'm not sure who wrote them
#define CL_CHECK(_expr)                                                         \
   do {                                                                         \
     cl_int _err = _expr;                                                       \
     if (_err == CL_SUCCESS)                                                    \
       break;                                                                   \
     fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err);   \
     abort();                                                                   \
   } while (0)

#define CL_CHECK_ERR(_expr)                                                     \
   ({                                                                           \
     cl_int _err = CL_INVALID_VALUE;                                            \
     typeof(_expr) _ret = _expr;                                                \
     if (_err != CL_SUCCESS) {                                                  \
       fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err); \
       abort();                                                                 \
     }                                                                          \
     _ret;                                                                      \
   })

int main (
  int                                 argc,
  char                         *argv[],
  char __attribute__((unused)) *envp[]
)
{
  // Commandline options parsing
  FILE *infile, *outfile;
  size_t infileSize;
  {
	int opterrors;
	struct stat instat;

	// Define the options to look for
 	struct arg_lit *help  = arg_lit0("h","help", 			"print this help and exit");
	struct arg_lit *version  = arg_lit0(NULL,"version",		"print version information and exit");
	struct arg_file *files = arg_filen(NULL,NULL,"<file>", 2, 2,	"input and output files");
 	struct arg_end *end = arg_end(20);
	void *argtable[] = {help, version, files, end};

	// Look for errors allocating the structures defined above
	if (arg_nullcheck(argtable) != 0)
		error (EXIT_FAILURE, 0, "Error allocating argtable structure");

	// Parse the command line
	opterrors = arg_parse(argc,argv,argtable);

 	// Process help flag
 	if (help->count > 0) {
		printf("Usage: %s", program_invocation_name);
		arg_print_syntaxv(stdout,argtable,"\n");
		printf("Compiles an OpenCL program, usually named *.cl, to create a binary output file, usually *.bin\n");
		arg_print_glossary(stdout,argtable,"  %-25s %s\n");
		exit(EXIT_SUCCESS);
        }

	// Process version flag
	if (version->count > 0) {
		printf("'%s' is a command that compiles an OpenCL kernel program\n",program_invocation_name);
		printf("November 2012, Craig Lund, BittWare\n");
		exit(EXIT_SUCCESS);
        }

	// If the parser returned any errors then display them and exit
	if (opterrors > 0) {
		arg_print_errors(stdout,end,program_invocation_name);
		error (EXIT_FAILURE, 0, "Try --help for more information");
        }

	// Validate the input file
	infile = fopen (files->filename[0], "rb");
	if (infile == NULL)
		error (EXIT_FAILURE, errno, "Error opening input file");
	if (fstat(fileno(infile), &instat) != 0)
		error (EXIT_FAILURE, errno, "Error doing an fstat() on the input file");
	infileSize = instat.st_size;
	if (infileSize < 4)
		error (EXIT_FAILURE, 0, "Input file size less than 4 bytes");

	// Open the output file
	outfile = fopen (files->filename[1], "wb");
	if (outfile == NULL) {
		error (EXIT_FAILURE, errno, "Error opening output file");
	}

	// Clean up options parsing
	arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
  }

  char *programBuffer; char *programLog;

  // get first available sdk and gpu and create context
  cl_platform_id platform;
  CL_CHECK( clGetPlatformIDs(1, &platform, NULL) );
  cl_device_id device;
  CL_CHECK( clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL) );
  cl_context context;
  context = CL_CHECK_ERR( clCreateContext(NULL, 1, &device, NULL, NULL, &_err) );

  // read kernel source into buffer
  programBuffer = (char*) malloc(infileSize + 1);
  programBuffer[infileSize] = '\0';
  { size_t result = fread(programBuffer, sizeof(char), infileSize, infile);
    if (result != infileSize)
    { perror ("Error reading program file");
      abort();
    }
  }
  fclose(infile);

  // create and build program
  cl_program program = CL_CHECK_ERR( clCreateProgramWithSource(context, 1, (const char**) &programBuffer, &infileSize, &_err) );
  free(programBuffer);


  cl_int build_error;
  build_error = clBuildProgram(program, 1, &device, "-Werror -cl-std=CL1.2 -cl-unsafe-math-optimizations -cl-fast-relaxed-math", NULL, NULL);
  if (build_error != CL_SUCCESS)
  { size_t logSize;
    cl_build_status status;

    // check build error and build status first
    CL_CHECK( clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &status, NULL) );

    // check build log
    CL_CHECK( clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize) );
    programLog = (char*) calloc (logSize+1, sizeof(char));
    CL_CHECK( clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, logSize+1, programLog, NULL) );
    printf("Build failed; error=%d, status=%d, programLog:\n\n%s", build_error, status, programLog);
    free(programLog);
    abort();
  }

  // Save program here

  // Determine how many different device binaries are in this program
  cl_uint numDevices = 0;
  CL_CHECK( clGetProgramInfo(program, CL_PROGRAM_NUM_DEVICES, sizeof(cl_uint), &numDevices, NULL) );

  // Get the list of devices
  cl_device_id *devices = malloc ( sizeof(cl_device_id) * numDevices );
  CL_CHECK( clGetProgramInfo(program, CL_PROGRAM_DEVICES, sizeof(cl_device_id) * numDevices, devices, NULL) );

  // Get sizes of the binaries
  size_t *programBinarySizes = malloc ( sizeof(size_t) * numDevices );
  CL_CHECK( clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES, sizeof(size_t) * numDevices, programBinarySizes, NULL) );

  // Create a data structure to hold the binaries
  unsigned char **programBinaries = malloc( sizeof(unsigned char *) * numDevices );
  for (cl_uint i = 0; i < numDevices; i++)
  {
    programBinaries[i] = malloc ( sizeof(unsigned char *) * programBinarySizes[i] );
  }

  // Fill that data structure with binaries
  CL_CHECK( clGetProgramInfo(program, CL_PROGRAM_BINARIES, sizeof(unsigned char *) * numDevices, programBinaries, NULL) );

  for (cl_uint i = 0; i < numDevices; i++)
  {
    // Store the binary just for the device requested.  In a scenario where
    // multiple devices were being used you would save all of the binaries out here.
    if (devices[i] == device)
    { fwrite(programBinaries[i], 1, programBinarySizes[i], outfile);
      fclose(outfile);
      break;
    }
  }

  // Cleanup
  free(devices);
  free(programBinarySizes);
  for (cl_uint i = 0; i < numDevices; i++)
  {
      free(programBinaries[i]);
  }
  free(programBinaries);


  CL_CHECK( clReleaseProgram(program) );
  CL_CHECK( clReleaseContext(context) );
  CL_CHECK( clReleaseDevice(device) );

  return EXIT_SUCCESS;
}
