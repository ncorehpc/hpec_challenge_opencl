// OpenCL static kernel language implementation of tdFIR for a GPU 
// $Id: simplerace.cl 9970 2013-01-09 20:00:52Z clund@localk.com $

// Multiply two complex numbers
__private inline float2 cmult
  ( __private float2 a, 
    __private float2 b
  )
{ return (__private float2)( a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

// Zero the output buffer
__kernel void tdFIRworkFirst
  ( __global float2 * const restrict OutputArray  // Length (N+K-1)*M
  )
{ __private size_t MElement = get_global_id(0);
  OutputArray[MElement] = (__private float2)(0.0f, 0.0f);
  return;
} // End tdFIRworkFirst


// Calculate the tdFIRwork
// In the example that I use for benchmarking M=64, N=4096, K=128
// That translates into InputBuffer=2M, FilterArray=64k, OutputBuffer=2.1M
// The AMD GPU I'm targeting has global=512M, local=32K, constant=64k

// This version has a race condition associated with OutputArray.
// The code happens to work with specific local work group sizes like (1, 1, 1) or (M, 1, 1) or (1, 256, 1)

__kernel void tdFIRworkMain
  ( __private  unsigned short     const                  N,           // Size of input in number of complex elements
    __private  unsigned short     const                  K,           // Size of filter in number of complex elements
    __global            float2    const * const restrict InputArray,  // Length N*M
    __constant          float2    const * const restrict FilterArray, // Length K*M which fits into my GPU's constant memory
    __global   volatile float2  * const         restrict OutputArray  // Length (N+K-1)*M
  )
{ 
  __private size_t MElement      = get_global_id(0);
  __private size_t InputElement  = get_global_id(1);
  __private size_t FilterElement = get_global_id(2);

  #define OutputBase  MElement*(N+K-1)
  #define InputBase   MElement*N
  #define FilterBase  MElement*K

  OutputArray[OutputBase+InputElement+FilterElement] +=
    cmult( InputArray[InputBase+InputElement], FilterArray[FilterBase+FilterElement]);

  return;
 
} // End tdFIRworkMain
